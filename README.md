# Master Smart Contract Development
Learn and implements tasks in solidity with test cases to master smart contract development

## Contracts Implmented
- [Lottery](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Loterry)
- [NFT Auction](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Auction)
- Proxy
    - [Transparent Proxy](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Proxy/Transparent)
    - [UUPS Proxy](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Proxy/UUPS)
- [Signature Verification](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/SignatureVerification)
- Security
    - [Reentrancy](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/Reentrancy)
    - [Arithmetic Overflow & Underflow](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/Arithmetic)
    - [Force Sending Ether](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/SelfDesctruct)
    - [DelegateCall](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/DelegateCall)
    - [Randomness](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/Randomness)
    - [Denial of Service](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/DenialOfService)
    - [tx.origin exploit](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/Phishing)
    - [Hiding Code](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/HidingMaliciousCode)
    - [HoneyPot](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/HoneyPot)
    - [Front Running](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/FrontRunning)
    - [Block Time Manipulation](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/BlockTimestamp)
    - [Signature Replay](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/SignatureReplay)
    - [Contract Size Check](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/Security/ContractSizeCheck)
- DeFi
    - [Vault](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/DeFi/Vault)
    - [Constant Product AMM](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/DeFi/AMM)
    - [Chainlink Oracle Integration](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/DeFi/Oracle)
    - [Staking Rewards](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/DeFi/Oracle)
    - [Uniswap V2 Integration](https://gitlab.com/gowtham_saravanan/master-smart-contracts/-/tree/main/contracts/DeFi/Uniswap)
